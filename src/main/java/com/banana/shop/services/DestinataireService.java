package com.banana.shop.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banana.shop.models.Destinataire;
import com.banana.shop.repositories.DestinataireRepository;

@Service
public class DestinataireService {

	@Autowired
	DestinataireRepository destinataireRepository;

	public List<Destinataire> getAllTimeDestinataires() {
		return destinataireRepository.findAll();
	}

	public List<Destinataire> getAllNonArchiveDestinataires() {
		return destinataireRepository.findAllByArchive(false);
	}

	public String saveDestinataire(Destinataire destinataire) {
		Optional<Destinataire> oDestinataire = destinataireRepository.findDuplicatedDestinataire(destinataire.getNom(), destinataire.getAdress(), destinataire.getCodePostal(), destinataire.getVille(), destinataire.getPays());
		if(oDestinataire.isPresent()) {
			return "Ce destinataire est déja enregister";
		}
		Optional<Destinataire> oDesti = destinataireRepository.findById(destinataire.getId());
		if (oDesti.isEmpty()) {
			destinataire.setArchive(false);
		}else {
			destinataire.setArchive(oDesti.get().getArchive());
		}
		String checkResult = checkDestinataire(destinataire);
		if("ok".equals(checkResult)) {
			destinataireRepository.save(destinataire);
			return "Commande saved correctly";
		}
		return checkResult;
	}

	public String deleteDestinataire(int id) {
		Optional<Destinataire> oDestinataire = destinataireRepository.findById(id);
		Destinataire destinataire = new Destinataire();
		if (oDestinataire.isEmpty()) {
			return "No destinataire to delete";
		}
		destinataire = oDestinataire.get();
		destinataire.setArchive(true);
		destinataireRepository.save(destinataire);
		return "destinataire deleted succecfully";
	}
	
	private String checkDestinataire(Destinataire destinataire) {
		if(destinataire.getNom()== null || destinataire.getNom().isBlank()) {
			return "Merci de renseigné un nom à votre destinataire";
		}
		if(destinataire.getAdress() == null || destinataire.getAdress().isBlank()) {
			return "Merci de renseigné une adresse à votre destinataire";
		}
		if(destinataire.getCodePostal()== null || destinataire.getCodePostal().isBlank()) {
			return "Merci de renseigné un code postal à votre destinataire";
		}
		if(destinataire.getPays() == null || destinataire.getPays().isBlank()) {
			return "Merci de renseigné un pays à votre destinataire";
		}
		if(destinataire.getVille() == null || destinataire.getVille().isBlank()) {
			return "Merci de renseigné une ville à votre destinataire";
		}
		return "ok";
	}
}
