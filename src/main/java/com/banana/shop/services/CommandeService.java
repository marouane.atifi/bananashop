package com.banana.shop.services;

import java.util.List;
import java.util.Optional;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banana.shop.models.Commande;
import com.banana.shop.models.Destinataire;
import com.banana.shop.repositories.CommandeRepository;
import com.banana.shop.repositories.DestinataireRepository;

@Service
public class CommandeService {

	@Autowired
	CommandeRepository commandeRepository;

	@Autowired
	DestinataireRepository destinataireRepository;

	public List<Commande> getAllTimeCommandes() {
		return commandeRepository.findAll();
	}

	public List<Commande> getAllNonArchiveCommandes() {
		return commandeRepository.findAllByArchive(false);
	}

	public List<Commande> getAllDestinatairesCommande(int id) {
		Optional<Destinataire> destinataire = destinataireRepository.findById(id);
		if (destinataire.isEmpty()) {
			return null;
		}
		return commandeRepository.findByDestinataire(destinataire.get());
	}

	public String saveCommande(Commande commande) {
		String checkResult = checkCommande(commande);
		if ("ok".equals(checkResult)) {
			Optional<Commande> oCommande = commandeRepository.findById(commande.getId());
			double quantite = commande.getQuantite();
			if (oCommande.isEmpty()) {
				commande.setArchive(false);
				commande.setQuantite(quantite * 25);
				commande.setPrix(quantite * 25 * 2.50);
			} else {
				commande.setArchive(oCommande.get().getArchive());
				if (commande.getQuantite() != oCommande.get().getQuantite()) {
					commande.setQuantite(quantite * 25);
					commande.setPrix(quantite * 25 * 2.50);
				} else {
					commande.setPrix(oCommande.get().getPrix());
				}
			}

			commandeRepository.save(commande);
			return "Commande saved correctly";
		}
		return checkResult;
	}

	public String deleteCommande(int id) {
		Optional<Commande> oCommande = commandeRepository.findById(id);
		Commande commande = new Commande();
		if (oCommande.isEmpty()) {
			return "No commande to delete";
		}
		commande = oCommande.get();
		commande.setArchive(true);
		commandeRepository.save(commande);
		return "Commande deleted succecfully";
	}

	private String checkCommande(Commande commande) {
		if (commande.getDateDeLivraison() == null
				|| commande.getDateDeLivraison().compareTo(LocalDate.now().plusDays(6)) < 0) {
			return "La date de livraison est incorrect ou inferieur à 7 jours";
		}
		if (commande.getQuantite() < 1 || commande.getQuantite() > 10000) {
			return "Merci de respecter les quantité à choisire (Entre 0 et 10 000)";
		}
		if (commande.getDestinataire() == null) {
			return "Merci de choisire un destinataire";
		}
		return "ok";
	}
}
