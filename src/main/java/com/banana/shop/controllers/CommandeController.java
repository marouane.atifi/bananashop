package com.banana.shop.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banana.shop.models.Commande;
import com.banana.shop.services.CommandeService;

@RestController
public class CommandeController {

	@Autowired
	CommandeService commandeService;

	@GetMapping("/Commandes")
	public List<Commande> getCommandes() {
		return commandeService.getAllNonArchiveCommandes();
	}

	@GetMapping("/allCommandes")
	public List<Commande> getAllTimeCommandes() {
		return commandeService.getAllTimeCommandes();
	}

	@PostMapping("/saveUpdateCommande")
	public String saveCommande(Commande commande) {
		return commandeService.saveCommande(commande);
	}

	@DeleteMapping("/deleteCommande")
	public String deleteCommande(int id) {
		return commandeService.deleteCommande(id);
	}
}
