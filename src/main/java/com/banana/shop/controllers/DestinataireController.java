package com.banana.shop.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banana.shop.models.Destinataire;
import com.banana.shop.services.DestinataireService;

@RestController
public class DestinataireController {

	@Autowired
	DestinataireService destinataireService;
	
	@GetMapping("/Destinataires")
	public List<Destinataire> getDestinataires(){
		return destinataireService.getAllNonArchiveDestinataires();
	}
	
	@GetMapping("/allDestinataires")
	public List<Destinataire> getAllTimeDestinataires(){
		return destinataireService.getAllTimeDestinataires();
	}
	
	@PostMapping("/saveUpdateDestinataire")
	public String saveDestinataire(Destinataire destinataire) {
		return destinataireService.saveDestinataire(destinataire);
	}
	
	@DeleteMapping("/deleteDestinataire")
	public String deleteDestinataire(int id) {
		return destinataireService.deleteDestinataire(id);
	}
}
