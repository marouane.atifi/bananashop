package com.banana.shop.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "commande")
@Setter
@Getter
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "id_destinataire", referencedColumnName = "id")
	private Destinataire destinataire;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateDeLivraison;
	private double quantite;
	private double prix;
	private Boolean archive;
}
