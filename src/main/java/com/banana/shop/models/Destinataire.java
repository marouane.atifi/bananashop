package com.banana.shop.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "destinataire")
@Setter
@Getter
public class Destinataire {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotBlank
	private String nom;
	@NotBlank
	private String adress;
	@NotBlank
	private String codePostal;
	@NotBlank
	private String ville;
	@NotBlank
	private String pays;
	private Boolean archive;
}
