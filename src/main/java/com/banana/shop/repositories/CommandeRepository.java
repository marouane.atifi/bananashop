package com.banana.shop.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banana.shop.models.Commande;
import com.banana.shop.models.Destinataire;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Integer> {

	List<Commande> findAllByArchive(boolean bool);
	
	List<Commande> findByDestinataire(Destinataire destinataire);

}
