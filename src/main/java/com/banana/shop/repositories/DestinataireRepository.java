package com.banana.shop.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.banana.shop.models.Destinataire;

@Repository
public interface DestinataireRepository extends JpaRepository<Destinataire, Integer> {

	List<Destinataire> findAllByArchive(boolean bool);
	
	@Query(value = "SELECT * FROM destinataire u WHERE u.nom = ?1 and u.adress = ?2 and u.code_postal = ?3 and u.ville = ?4 and u.pays = ?5", 
			  nativeQuery = true)
	Optional<Destinataire> findDuplicatedDestinataire(String nom, String adress, String codePostal,String ville,String pays);
	
}
